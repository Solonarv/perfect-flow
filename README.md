# perfect-flow
A kind of rhythm game disguised as WoW-style combat.

## How to install

Download the [itch.io app](https://itch.io/app), then visit [solonarv.itch.io/perfect-flow](https://solonarv.itch.io/perfect-flow) and download the game.
You can then launch the game from the itch.io app.

If you don't want to use the itch.io app, you can also download the appropriate ZIP for your platform from [solonarv.itch.io/perfect-flow](https://solonarv.itch.io/perfect-flow),
extract it in a folder of your choosing, and launch the `perfect-flow` executable.